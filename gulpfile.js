var elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    /* Vendor files */
    mix.styles([
        'bootstrap/dist/css/bootstrap.css',
        'bootstrap-tokenfield/dist/css/bootstrap-tokenfield.css',
        'bootstrap-tokenfield/dist/css/tokenfield-typeahead.css',
        'datatables/media/css/jquery.dataTables.css',
        'dropzone/dist/dropzone.css',
        'font-awesome/css/font-awesome.css',
        'nouislider/distribute/nouislider.css',
        '../resources/assets/vendor/css/jquery-ui.css'
    ], 'public/css/vendor.css', 'node_modules');

    mix.copy([
        'node_modules/bootstrap/fonts',
        'node_modules/font-awesome/fonts'
    ], 'public/fonts');

    mix.copy('node_modules/jquery/dist/jquery.js','public/js');
    mix.copy('resources/assets/vendor/js/jquery-ui.js','public/js');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.js','public/js');
    mix.copy('node_modules/vue/dist/vue.js','public/js');
    mix.copy('node_modules/vue-resource/dist/vue-resource.js','public/js');

    mix.scripts([
        'datatables/media/js/jquery.dataTables.js',
        'dropzone/dist/dropzone.js',
        'moment/moment.js',
        'bootstrap-tokenfield/dist/bootstrap-tokenfield.js',
        'nouislider/distribute/nouislider.js'
    ], 'public/js/vendor.js', 'node_modules');

    /* App files */
    mix.copy('resources/assets/images', 'public/images');
    mix.styles([
        'app.css'
    ]);
    mix.scripts([
        'app.js'
    ]);

});
