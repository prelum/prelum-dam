# Prelum DAM #

# Framework #

Deze applicatie is gebouwd op Laravel 5.4. Ga naar [de documentatie](https://laravel.com/docs/5.4) om een inzicht te krijgen over de verschillende mogelijkheden.

Onderstaande instructies vormen een uitbreiding op de mogelijkheden zoals daar gespecificieerd.

## Installatie ##
### Development ###
Met deze repo is er een [Vagrant box](https://box.scotch.io/) meegeleverd die dezelfde packages heeft als de doelserver. Installeer dus eerst [VirtualBox](https://www.virtualbox.org/) en [Vagrant](https://www.vagrantup.com/).

Doe vervolgens

```
vagrant up
```

om de box te activeren en

```
vagrant ssh
```

om in te loggen in de box. de webfiles bevinden zicht dan onder /var/www, en de website is aan te roepen onder 192.168.33.10.

#### Mailcatcher ####
Er is ook een mail-receiver meegeleverd met de box. Deze wordt automatisch geinstalleerd. Om deze te activeren moet

```
vagrant@scotchbox:/var/www$ mailcatcher --http-ip=0.0.0.0
```

gedraaid worden. Met de instellingen meegeleverd in de .env.example zullen alle mails naar deze tool gestuurd worden. Zij zijn dan beschikbaar onder http://192.168.33.10:1080.

#### Database ####

De database kan met een simpele

```
php artisan migrate --seed
```

gevuld worden. Er worden hierbij ook direct twee users aangemaakt

- Een admin-user (admin@prelum.nl / admin)
- Een reguliere gebruiker (user@prelum.nl / admin)

Het is ook mogelijk om apart een aantal assets in te laten schieten:

```
php artisan db:seed --class=AssetsSeeder
```


### Media Assets ###

Alle assets worden geladen door middel van [gulp](http://gulpjs.com/).

Het is mogelijk om gulp de veranderde code te laten monitoren, zodat eventuele veranderingen direct doorgevoerd worden.

```
gulp watch
```

**Let op!** Doe dit alleen in development.

### Acceptatie en productie ###

Het updaten van de acceptatie- en productieomgeving gebeurt via FTP op een lokale. Zie LastPass voor de inloggegevens hiervan.
In de meegeleverde PhpStorm-instellingen worden de vendor en node_modules map niet automatisch meegesynchroniseerd. Let er dus op dat je ook

```
composer update && npm update && gulp
```

moet draaien (naast eventuele [Laravel artisan calls](https://laravel.com/docs/5.4/artisan) ) om alle libraries up to date te krijgen.

## Overig ##

Zie voor de overige documentatie de PHP docs in de code.