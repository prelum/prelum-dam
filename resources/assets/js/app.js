$(document).ready(function () {
    $(".dropdown-toggle").dropdown();
});

function sendDataUpdate(formID) {
    var form = $('#' + formID);
    console.log(form.serializeArray());
    $.ajax({
        url: form.attr('data-url'),
        method: form.attr('data-method'),
        data: form.serializeArray(),
        dataType: 'json',
        statusCode: {
            201: function (data) {
                console.log(data);
                handleSuccess(data);
            },
            422: function (data) {
                console.log(data);
                handleValidationError(formID, data);
            }
        }
    });
}

function sendModalDataUpdate(formID, modalID) {
    var form = $('#' + formID);
    $.ajax({
        url: form.attr('data-url'),
        method: form.attr('data-method'),
        data: form.serializeArray(),
        dataType: 'json',
        statusCode: {
            201: function (data) {
                handleModalSuccess(modalID, data);
                location.reload();
            },
            422: function (data) {
                handleValidationError(formID, data);
            }
        }
    });
}

function handleSuccess(data) {
    console.log(data);
    $(window).attr('location', data.redirect)
}

function handleModalSuccess(modalID, data) {
    console.log(data);
    $('#' + modalID).modal('toggle');
}

function handleValidationError(formID, data) {
    console.log(data);
    $('#' + formID + " > [id$=-input]").removeClass("has-error");
    $("[id$=-errors]").empty();
    for (var errorKey in data.responseJSON) {
        $('#' + formID + " > #" + errorKey + '-input').addClass("has-error");
        $('#' + formID + " > #" + errorKey + '-input' + " > #" + errorKey + '-errors').append('<div class="alert alert-danger">' + data.responseJSON[errorKey] + '</div>');
    }
}

function formatBytes(bytes, decimals) {
    if (bytes == 0) return '0 Bytes';
    var k = 1000,
        dm = decimals + 1 || 3,
        sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
