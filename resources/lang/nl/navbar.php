<?php

return [
    'userprofile' => 'Profiel',
    'dashboard' => 'Dashboard',
    'logout' => 'Uitloggen',
    'upload' => 'Uploaden',
    'reports' => 'Rapportage',
    'assets' => 'Mediabank',
    'admin' => [
        'head' => 'Administratie',
        'users' => 'Gebruikers',
        'products' => 'Producten',
        'tags' => 'Tags'
    ]
];
