<?php
return [
    'users' => [
        'page-title' => 'Users',
        'overview' => [
            'title' => 'Gebruikers',
            'id' => 'ID',
            'first-name' => 'Voornaam',
            'last-name' => 'Achternaam',
            'email' => 'Emailadres',
            'role' => 'Type'
        ]
    ],
    'products' => [
        'page-title' => 'Products',
        'overview' => [
            'title' => 'Producten',
            'productcode' => 'Code',
            'name' => 'Naam',
            'website' => 'Website'
        ]
    ],
    'tags' => [
        'page-title' => 'Tags',
        'overview' => [
            'title' => 'Tags',
            'id' => 'ID',
            'name' => 'Tag',
            'count' => 'Aantal keer gebruikt',
        ]
    ]
];