<?php

return [
    'login' => [
        'title' => 'Log in',
        'email' => 'E-mail',
        'password' => 'Wachtwoord',
        'login-button' => 'Inloggen',
        'forgot-password' => 'Wachtwoord vergeten?',
        'failed' => 'Deze combinatie van gebruiker en wachtwoord is niet gevonden.',
        'throttle' => 'U heeft meerdere malen foutief in proberen loggen. Graag opnieuw proberen in :seconds seconden.',
    ],
    'email' => [
        'title' => 'Reset wachtwoord',
        'email' => 'E-mail',
        'send-link' => 'Verzenden'
    ],
    'reset' => [
        'title' => 'Reset wachtwoord',
        'email' => 'E-mail',
        'password' => 'Wachtwoord',
        'password-repeat' => 'Herhaal wachtwoord',
        'reset-button' => 'Vervang wachtwoord'
    ]
];
