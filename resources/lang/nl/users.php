<?php

return [
    'page-title' => 'Mijn account',
    'details' => [
        'title' => 'Mijn gegevens',
        'editbutton' => 'Aanpassen',
        'name' => 'Naam: :first_name :last_name',
        'email' => 'Emailadres: :email',
        'role' => 'Rol: :role'
    ]
];