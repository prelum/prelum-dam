<?php
return [
    'index' => [
        'page-title' => 'Mediabank',
        'filter' => [
            'title' => 'Filters',
            'search-button' => 'Zoeken',
            'search-text' => 'Zoek naar:',
            'search-placeholder' => 'Bv. "Test"',
            'type' => [
                'title' => 'Mediatype',
                'image' => 'Afbeelding',
                'video' => 'Video',
                'audio' => 'Audio',
                'document' => 'Document'
            ],
            'size' => [
                'title' => 'Bestandsgrootte',
                'small' => 'Klein',
                'medium' => 'Gemiddeld',
                'large' => 'Groot',

            ],
            'availability' => [
                'title' => 'Beschikbaarheid',
                'yes' => 'Ja',
                'no' => 'Nee',
                'contact' => 'Contact opnemen',
                'unknown' => 'Onbekend'

            ],
            'products' => 'Producten',
            'creators' => 'Creators',

        ],
        'asset' => [
            'dimensions' => ':height x :width',
        ],
        'set-size' => [
            'name' => 'Aantal resultaten'

        ],
        'ordering' => [
            'name' => 'Volgorde',
            'small-to-large' => 'Klein naar groot',
            'large-to-small' => 'Groot naar klein',
            'last-to-earliest' => 'Oudste eerst',
            'earliest-to-last' => 'Nieuwste eerst'
        ]

    ],
    'asset' => [
        'page-title' => ':assetName',
        'image' => [
            'downloadbtn' => 'Downloaden'
        ],
        'overview' => [
            'panel-title' => 'Gegevens',
            'metadata' => [
                'title' => 'Metadata',
                'name' => 'Naam: :name',
                'user' => 'Toegevoegd door: :first_name :last_name',
                'created' => 'Aangemaakt: :created_at',
                'updated' => 'Aangepast: :updated_at',
                'original-name' => 'Originele naam: :original-name',
                'description' => 'Omschrijving: :description'
            ],
            'specifications' => [
                'title' => 'Technische gegevens',
                'extension' => 'Type: :extension',
                'size' => 'Grootte: :size',
                'dimensions' => 'Dimensies: H :height x B :width pixels',
            ],
            'productdata' => [
                'title' => 'Gegevens bezittend product',
                'name' => 'Productnaam: :product_name'
            ],
            'availability' => [
                'title' => 'Beschikbaarheidsgegevens',
                'available' => 'Beschikbaarheid: :available'
            ],
            'creator' => [
                'title' => 'Creator',
                'name' => 'Naam: :first_name :last_name',
                'function' => 'Functie: :function',
                'email' => 'Email: :email'
            ],
            'tags' => [
                'title' => 'Tags'
            ]
        ],
        'comments' => [
            'panel-title' => 'Opmerkingen',
            'none' => 'Er zijn geen opmerkingen beschikbaar',
            'creator' => 'Auteur',
            'createddate' => 'Aangemaakt',
            'updateddate' => 'Aangepast'
        ],
        'history' => [
            'panel-title' => 'Geschiedenis',
            'base' => ':first_name :last_name :type deze media-asset op :time',
            'createdField' => ':field is gedefiniëerd als :newValue',
            'updatedField' => ':field was :newValue en is gedefiniëerd als :newValue'
        ],
    ]
];