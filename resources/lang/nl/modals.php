<?php

return [
    'asset' => [
        'title' => 'Gegevens aanpassen',
        'products' => 'Product',
        'name' => 'Naam',
        'description' => 'Omschrijving',
        'creator' => 'Creator',
        'tags' => 'Tags',
        'available' => [
            'title' => 'Beschikbaarheid',
            'yes' => 'Ja',
            'no' => 'Nee',
            'contact' => 'Contact',
            'unknown' => 'Onbekend'
        ],
    ],
    'comment' => [
        'title' => 'Nieuwe opmerking toevoegen',
        'comment-title' => 'Titel',
        'text' => 'Tekst',
    ],
    'creator' => [
        'title' => 'Nieuwe creator toevoegen',
        'first-name' => 'Voornaam',
        'last-name' => 'Achternaam',
        'function' => 'Functie',
        'email' => 'Email'
    ],
    'user' => [
        'title' => 'Gebruiker aanpassen',
        'first-name' => 'Voornaam',
        'last-name' => 'Achternaam',
        'email' => 'Emailadres',
        'password' => 'Wachtwoord',
        'password-repeat' => 'Herhaal wachtwoord',
        'roles' => 'Rol'
    ],
    'product' => [
        'title' => 'Product aanpassen',
        'productcode' => 'ProductCode',
        'name' => 'Productnaam',
        'website' => 'Website'
    ],
    'destroy' => [
        'title' => 'Record verwijderen',
        'text' => 'Bent u zeker dat u dit record wilt verwijderen?'
    ],
    'close' => 'Sluiten',
    'store' => 'Opslaan',
    'update' => 'Aanpassen',
    'delete' => 'Verwijderen'
];