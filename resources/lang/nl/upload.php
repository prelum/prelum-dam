<?php

return [
    'dropzone' => [
        'messages' => [
            'default' => 'Sleep hier uw plaatjes heen, of klik op dit veld om plaatjes uit uw bestanden toe te voegen.',
            'remove-file' => 'Verwijder bestand',
            'invalid-type' => 'Deze extensie wordt niet ondersteund'
        ],
        'upload-button' => 'Uploaden',
        'page-title' => 'Uploads dropzone',
        'warning' => 'Opgelet!',
        'warning-size' => 'max 50 MB totale grootte',
        'warning-number' => 'max 20 plaatjes per keer'
    ],
    'batch' => [
        'page-title' => 'Bulk upload',
        'form-title' => 'Algemene gegevens',
        'products' => 'Producten',
        'creator' => 'Naam creator',
        'copyright' => 'Copyright',
        'comment' => [
            'title' => 'Opmerkingen',
            'comment-title' => 'Titel',
            'comment' => 'Tekst',
            'button-add' => 'Toevoegen'
        ],
        'tags' => 'Tags',
        'available' => [
            'title' => 'Beschikbaarheid',
            'yes' => 'Ja',
            'no' => 'Nee',
            'contact' => 'Contact opnemen',
            'unknown' => 'Onbekend'
        ],
        'upload-button' => 'Uploaden',
        'queue' => [
            'length' => 'Er staan :queueCount bestanden in de wachtrij om verwerkt te worden.'
        ]
    ],
    'individual' => [
        'comment-title' => 'Titel opmerking',
        'comment' => 'Tekst',
        'queue' => [
            'empty' => 'Er staan geen media-bestanden meer in de wachtrij.',
            'single' => 'Er staat nog :queueCount media-bestand in de wachtrij.',
            'multiple' => 'Er staan nog :queueCount media-bestanden in de wachtrij.'
        ],
        'asset' => [
            'title' => 'Algemene gegevens',
            'products' => 'Producten',
            'asset-name' => 'Naam media-bestand',
            'description' => 'Omschrijving',
            'creator' => 'Naam creator',
            'copyright' => 'Copyright',
            'comment' => 'Opmerking',
            'tags' => 'Tags',
            'available' => [
                'title' => 'Beschikbaarheid',
                'yes' => 'Ja',
                'no' => 'Nee',
                'contact' => 'Contact opnemen',
                'unknown' => 'Onbekend'
            ],
        ],
        'upload-button' => 'Uploaden',
        'specifications' => [
            'panel' => 'Media-bestand',
            'title' => 'Bestandseigenschappen',
            'size' => 'Bestandsgrootte: :size bytes',
            'height' => 'Hoogte: :height pixels',
            'width' => 'Breedte: :width pixels',
            'dimensions' => 'Afmetingen :height  x :width pixels',
            'iptc' => 'Heeft iptc-gegevens',
            'exif' => 'Heeft exifgegevens'
        ]
    ]
];