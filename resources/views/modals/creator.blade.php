<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">{{ trans('modals.creator.title') }}</h4>
        </div>
        <div class="modal-body">
            <form id="creatorForm"
                  data-method="{{isset($creator) ? 'put' : 'post'}}"
                  data-url="{{isset($creator) ? url('creators/'. $creator->id) : url('creators/')}}"
                  action="javascript:void(0);">
                {!! csrf_field() !!}
                <div class="form-group" id="first-name-input">
                    <label for="first-name">{{ trans('modals.creator.first-name') }}</label>
                    <div id="first-name-errors"></div>
                    <input class="form-control" id="first-name" name="first-name" type="text"
                           value="{{ $creator->first_name or null }}">
                </div>
                <div class="form-group" id="last-name-input">
                    <label for="last-name">{{ trans('modals.creator.last-name') }}</label>
                    <div id="last-name-errors"></div>
                    <input class="form-control" id="last-name" name="last-name" type="text"
                           value="{{ $creator->last_name or null }}">
                </div>
                <div class="form-group" id="function-input">
                    <label for="function">{{ trans('modals.creator.function') }}</label>
                    <div id="function-errors"></div>
                    <input class="form-control" id="function" name="function" type="text"
                           value="{{ $creator->function or null }}">
                </div>
                <div class="form-group" id="email-input">
                    <label for="email">{{ trans('modals.creator.email') }}</label>
                    <div id="email-errors"></div>
                    <input class="form-control" id="email" name="email" type="text"
                           value="{{ $creator->email or null }}">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default"
                    data-dismiss="modal">{{ trans('modals.close') }}</button>
            <button class="btn-primary btn" type="submit"
                    id="btnCreator" form="creatorForm">
                @if(isset($creator))
                    {{ trans('modals.update') }}
                @else
                    {{ trans('modals.store') }}
                @endif
            </button>
        </div>
    </div>
</div>
<script>
    $('#btnCreator').on('click', function () {
        sendModalDataUpdate('creatorForm', 'creatorModal');
    });
</script>