<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">{{ trans('modals.asset.title') }}</h4>
        </div>
        <div class="modal-body">
            <form id="assetForm"
                  data-method="{{isset($asset) ? 'put' : 'post'}}"
                  data-url="{{isset($asset) ? url('assets/'. $asset->id) : url('assets/')}}"
                  action="javascript:void(0);">
                {!! csrf_field() !!}
                <div class="form-group" id="product_id-input">
                    <label for="products">{{ trans('modals.asset.products') }}</label>
                    <div id="product_id-errors"></div>
                    <div class="input-group">
                        <select id="products" name="product_id" class="form-control">
                            @foreach($products as $product)
                                @if($product->id == $asset->product->id)
                                    <option value="{{$product->id}}" selected>{{$product->name}}</option>
                                @else
                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                @endif
                            @endforeach
                        </select>
                        <div class="input-group-btn">
                            <button class="btn btn-default"
                                    type="button"
                                    id="productModalBtn">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>

                        </div>
                    </div>
                </div>
                <div class="form-group" id="creator_id-input">
                    <label for="creators">{{ trans('modals.asset.creator') }}</label>
                    <div id="creator_id-errors"></div>
                    <div class="input-group">
                        <select id="creators" name="creator_id" class="form-control">
                            <option></option>
                            @foreach($creators as $creator)
                                @if($creator->id == $asset->creator->id)

                                    <option value="{{$creator->id}}"
                                            selected>{{$creator->getFullName()}}</option>
                                @else
                                    <option value="{{$creator->id}}">{{$creator->getFullName()}}</option>
                                @endif
                            @endforeach
                        </select>
                        <div class="input-group-btn">
                            <button class="btn btn-default"
                                    type="button"
                                    id="creatorModalBtn">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>

                        </div>
                    </div>

                </div>
                <div class="form-group" id="name-input">
                    <label for="name">{{ trans('modals.asset.name') }}</label>
                    <div id="name-errors"></div>
                    <input class="form-control" id="name" name="name" type="text"
                           value="{{$asset->name}}">
                </div>
                <div class="form-group" id="description-input">
                    <label for="description">{{ trans('modals.asset.description') }}</label>
                    <div id="description-errors"></div>
                    <textarea class="form-control" id="description" name="description"
                              rows="5">{{$asset->description}}</textarea>
                </div>

                <div class="form-group" id="tags-input">
                    <label for="tags">{{ trans('modals.asset.tags') }}</label>
                    <input class="form-control" id="tags" name="tags" data-role="tagsinput"
                           value="{{$asset->stringifyTags()}}">
                </div>
                <div class="form-group" id="available-input">
                    <label>{{ trans('modals.asset.available.title') }}</label>
                    <div id="available-errors"></div>
                    @foreach($available as $type)
                        @if($asset->available == $type)
                            <div class="radio">
                                <label><input type="radio" id="available-{{$type}}" name="available"
                                              value="{{$type}}" checked>{{ trans('modals.asset.available.' . $type) }}
                                </label>
                            </div>
                        @else
                            <div class="radio">
                                <label><input type="radio" id="available-{{$type}}" name="available"
                                              value="{{$type}}">{{ trans('modals.asset.available.' . $type) }}</label>
                            </div>
                        @endif
                    @endforeach
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default"
                    data-dismiss="modal">{{ trans('modals.close') }}</button>
            <button class="btn-primary btn" type="submit"
                    id="btnAsset" form="assetForm">
                @if(isset($asset))
                    {{ trans('modals.update') }}
                @else
                    {{ trans('modals.store') }}
                @endif
            </button>
        </div>
    </div>
</div>
<div class="modal fade" id="creatorModal" role="dialog" data-url="{{url('/creators/create')}}"></div>
<div class="modal fade" id="productModal" role="dialog" data-url="{{url('/products/create')}}"></div>
<script>
    $(function () {
        $('#btnAsset').on('click', function () {
            sendModalDataUpdate('assetForm', 'assetModal');
        });
        $('#creatorModalBtn').on('click', function (e) {
            var modal = $('#creatorModal');
            var link = modal.data('url');
            modal.load(link);
            modal.modal();
        });

        $('#productModalBtn').on('click', function (e) {
            var modal = $('#productModal');
            var link = modal.data('url');
            modal.load(link);
            modal.modal();
        });
        $('#tags').tokenfield({
            autocomplete: {
                source: {!! $tags !!},
                delay: 0
            },
            showAutocompleteOnFocus: true
        });
    });
</script>