<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">{{ trans('modals.destroy.title') }}</h4>
        </div>
        <div class="modal-body">
            {{ trans('modals.destroy.text') }}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default"
                    data-dismiss="modal">{{ trans('modals.close') }}</button>
            <button class="btn-primary btn" type="submit"
                    id="btnDelete"
                    data-url="{{$url}}"
                    data-token="{{ csrf_token() }}">
                {{ trans('modals.delete') }}
            </button>
        </div>
    </div>
</div>
<script>
    $('#btnDelete').on('click', function (e) {
        $.ajax({
            url: $('#btnDelete').data('url'),
            type: 'DELETE',
            data: {
                _token: $('#btnDelete').data('token')
            },
            statusCode: {
                202: function (data, textStatus, jqXHR) {
                    console.log(data);
                    window.location.replace(data.redirect);
                }
            }
        });
    });
</script>