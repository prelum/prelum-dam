<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">{{ trans('modals.user.title') }}</h4>
        </div>
        <div class="modal-body">
            <form id="userForm"
                  data-method="{{isset($user) ? 'put' : 'post'}}"
                  data-url="{{isset($user) ? url('users/'. $user->id) : url('users/')}}"
                  action="javascript:void(0);">
                {!! csrf_field() !!}
                <div class="form-group" id="first_name-input">
                    <label for="first_name">{{ trans('modals.user.first-name') }}</label>
                    <div id="first_name-errors"></div>
                    <input class="form-control" id="first_name" name="first_name" type="text"
                           value="{{$user->first_name or null}}">
                </div>
                <div class="form-group" id="last_name-input">
                    <label for="last_name">{{ trans('modals.user.last-name') }}</label>
                    <div id="last_name-errors"></div>
                    <input class="form-control" id="last_name" name="last_name" type="text"
                           value="{{$user->last_name or null}}">
                </div>
                <div class="form-group" id="email-input">
                    <label for="email">{{ trans('modals.user.email') }}</label>
                    <div id="email-errors"></div>
                    <input class="form-control" id="email" name="email" type="text"
                           value="{{$user->email or null}}">
                </div>
                <hr>
                <div class="form-group" id="password-input">
                    <label for="password">{{ trans('modals.user.password') }}</label>
                    <div id="password-errors"></div>
                    <input class="form-control" id="password" name="password" type="password"
                           value="">
                </div>
                <div class="form-group" id="password-repeat-input">
                    <div id="password-repeat-errors"></div>
                    <label for="password-repeat">{{ trans('modals.user.password-repeat') }}</label>
                    <input class="form-control" id="password-repeat" name="password-repeat" type="password"
                           value="">
                </div>
                <div class="form-group" id="role-input">
                    <label for="role">{{ trans('modals.user.roles') }}</label>
                    <div id="role-errors"></div>
                    <select class="form-control" id="role" name="role">
                        @foreach($roles as $role)
                            @if(!empty($user) && $user->hasRole($role))
                                <option value="{{$role}}" selected>{{$role}}</option>
                            @else
                                <option value="{{$role}}">{{$role}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </form>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default"
                    data-dismiss="modal">{{ trans('modals.close') }}</button>
            <button class="btn-primary btn" type="submit"
                    id="btnUser" form="userForm">
                @if(isset($user))
                    {{ trans('modals.update') }}
                @else
                    {{ trans('modals.store') }}
                @endif
            </button>
        </div>
    </div>
</div>
<script>
    $('#btnUser').on('click', function () {
        console.log('Hier');
        sendModalDataUpdate('userForm', 'userModal');
    });
</script>