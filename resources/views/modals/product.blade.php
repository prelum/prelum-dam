<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">{{ trans('modals.product.title') }}</h4>
        </div>
        <div class="modal-body">
            <form id="productForm"
                  data-method="{{isset($product) ? 'put' : 'post'}}"
                  data-url="{{isset($product) ? url('products/'. $product->id) : url('products/')}}"
                  action="javascript:void(0);">
                {!! csrf_field() !!}
                <div class="form-group" id="productCode-input">
                    <label for="productCode">{{ trans('modals.product.productcode') }}</label>
                    <div id="productCode-errors"></div>
                    <input class="form-control" id="productCode" name="productCode" type="text"
                           value="{{$product->productCode or null}}">
                </div>
                <div class="form-group" id="name-input">
                    <label for="name">{{ trans('modals.product.name') }}</label>
                    <div id="name-errors"></div>
                    <input class="form-control" id="name" name="name" type="text"
                           value="{{$product->name or null}}">
                </div>
                <div class="form-group" id="website-input">
                    <label for="website">{{ trans('modals.product.website') }}</label>
                    <div id="website-errors"></div>
                    <input class="form-control" id="website" name="website" type="text"
                           value="{{$product->website or null}}">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default"
                    data-dismiss="modal">{{ trans('modals.close') }}</button>
            <button class="btn-primary btn" type="submit"
                    id="btnProduct" form="productForm">
                @if(isset($product))
                    {{ trans('modals.update') }}
                @else
                    {{ trans('modals.store') }}
                @endif
            </button>
        </div>
    </div>
</div>
<script>
    $('#btnProduct').on('click', function () {
        sendModalDataUpdate('productForm', 'productModal');
    });
</script>