<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">{{ trans('modals.comment.title') }}</h4>
        </div>
        <div class="modal-body">
            <form id="commentForm"
                  data-method="{{isset($comment) ? 'put' : 'post'}}"
                  data-url="{{isset($comment) ? url('assets/'. $uuid. '/comments/'. $comment->id) : url('assets/'. $uuid. '/comments/')}}"
                  action="javascript:void(0);">
                {!! csrf_field() !!}
                <div class=" form-group" id="title-input">
                    <label for="comment-title">{{ trans('modals.comment.comment-title') }}</label>
                    <div id="title-errors"></div>
                    <input class="form-control" id="comment-title" type="text" name="title"
                           value="{{ $comment->title or null }}">
                </div>
                <div class="form-group" id="text-input">
                    <label for="comment-text">{{ trans('modals.comment.text') }}</label>
                    <div id="text-errors"></div>
                                <textarea class="form-control" id="comment-text" name="text"
                                          rows="5">{{ $comment->text or null }}</textarea>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default"
                    data-dismiss="modal">{{ trans('modals.close') }}</button>
            <button class="btn-primary btn" type="submit"
                    id="btnComment" form="commentForm">
                @if(isset($comment))
                    {{ trans('modals.update') }}
                @else
                    {{ trans('modals.store') }}
                @endif
            </button>
        </div>
    </div>
</div>
<script>
    $('#btnComment').on('click', function () {
        sendModalDataUpdate('commentForm', 'commentModal');
    });
</script>