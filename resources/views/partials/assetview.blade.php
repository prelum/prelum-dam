@inject('converter', 'App\Services\ViewDataConversionService')

<div class="row" style="display:flex; flex-wrap: wrap; margin-bottom: 10px;">
    @foreach($assets as $key =>$asset)
        <div class="col-lg-{{12 / $assetsPerRow}}">
            <div class="thumbnail equalize">
                <div class="caption">
                    <p class="text-center"><strong>{{$asset->name}}</strong></p>
                </div>
                <a href="{{url('assets/' . $asset->id)}}">
                    <img class="img-thumbnail img-responsive center-block"
                         src={{url($asset->getThumbnailLocation())}}
                                 alt="{{$asset->name}}">

                </a>
                <div class="caption">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="text-center">{{$converter->formatBytes($asset->size, 0)}} | {{$asset->extension}}
                                | {{ trans('assets.index.asset.dimensions', ['height' => $asset->assetable->height, 'width' =>$asset->assetable->width])}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($key % $assetsPerRow == $assetsPerRow - 1)
</div>
<div class="row" style="margin-bottom: 10px;">
    @endif
    @endforeach
</div>
<div class="row">
    <div class="text-center">
        {!! $assets->render() !!}
    </div>
</div>

<script>
    $(document).ready(function() {
        var maxHeight = 0;
        $(".equalize").each(function(){
            if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
        });
        $(".equalize").height(maxHeight);
    });
</script>