<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('images/dvk_logo.png')}}"></a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <!--<li><a href="{{url('/')}}">{{ trans('navbar.dashboard') }}</a></li>-->
            <li><a href="{{url('assets')}}">{{ trans('navbar.assets') }}</a></li>
            <li><a href="{{url('upload')}}">{{ trans('navbar.upload') }}</a></li>
        <!--<li><a href="{{url('reports')}}">{{ trans('navbar.reports') }}</a></li>-->
            @if(Auth::user()->hasRole('admin'))
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">{{ trans('navbar.admin.head') }} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('admin/users')}}">{{ trans('navbar.admin.users') }}</a></li>
                        <li><a href="{{url('admin/products')}}">{{ trans('navbar.admin.products') }}</a></li>
                        <li><a href="{{url('admin/tags')}}">{{ trans('navbar.admin.tags') }}</a></li>
                    </ul>
                </li>
                <li><a href="{{url('admin')}}"></a></li>
            @endif
        </ul>
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user fa-fw"></i> <i
                            class="fa fa-caret-down"></i></a>
                <ul class="dropdown-menu dropdown-user">
                    <!--<li><a href="{{url('users/' . Auth::user()->id)}}"><i
                                    class="fa fa-user fa-fw"></i> {{ trans('navbar.userprofile') }}</a></li>
                    <li class="divider"></li>-->
                    <li>
                        <a href="{{url('logout')}}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                    class="fa fa-sign-out fa-fw"></i> {{ trans('navbar.logout') }}
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>