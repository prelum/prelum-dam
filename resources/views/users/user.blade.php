@extends('master')

@section('title')
    {{ trans('users.page-title') }}
@endsection

@section('navigation')
    @include('partials.navbar')
@endsection

@section('content')
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                {{ trans('users.details.title') }}
                <button class="btn btn-default btn-xs pull-right"
                        data-target="#userUpdateModal"
                        data-url="{{url('users/'. $user->id. '/edit')}}">
                    <span class="glyphicon glyphicon-pencil"></span>
                </button>
            </div>
            <div class="panel-body">
                <p>{{ trans('users.details.name', ['first_name' => $user->first_name, 'last_name' => $user->last_name]) }}</p>
                <p>{{ trans('users.details.email', ['email' => $user->email]) }}</p>
                <p>{{ trans('users.details.role', ['role' => $user->role]) }}</p>
            </div>
        </div>
    </div>
    <div class="modal fade" id="userUpdateModal" role="dialog"></div>
@endsection
@section('scripts')
    <script>
        $("button[data-target$='Modal']").on('click', function (e) {
            var modal = $($(this).data('target'));
            var link = $(this).data('url');
            console.log(link);
            modal.load(link);
            modal.modal();
        });
    </script>
@append