@extends('master')

@section('title')
    {{ trans('upload.batch.page-title') }}
@endsection

@section('navigation')
    @include('partials.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('upload.batch.form-title') }}
                </div>
                <div class="panel-body">
                    <div class="alert alert-info" role="alert">
                        {{ trans('upload.batch.queue.length', ['queueCount' => count($queue)]) }}
                    </div>
                    <form id="massUploadForm" role="form" data-method="post" data-url="{{url('/upload/batch')}}"
                          action="javascript:void(0);">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="products">{{ trans('upload.batch.products') }}</label>
                            <div class="input-group">
                                <select id="products" name="product_id" class="form-control">
                                    <option selected></option>
                                    @foreach($products as $product)
                                        <option value="{{$product->id}}">{{$product->name}}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-btn">
                                    <button class="btn btn-default"
                                            type="button"
                                            id="productModalBtn">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="creators">{{ trans('upload.batch.creator') }}</label>
                            <div class="input-group">
                                <select id="creators" name="creator_id" class="form-control">
                                    <option selected></option>
                                    @foreach($creators as $creator)
                                        <option value="{{$creator->id}}">{{$creator->getFullName()}}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-btn">
                                    <button class="btn btn-default"
                                            type="button"
                                            id="creatorModalBtn">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tags">{{ trans('upload.batch.tags') }}</label>
                            <input id="tags" name="tags" data-role="tagsinput">
                        </div>
                        <div class="form-group">
                            <label>{{ trans('upload.batch.available.title') }}</label>
                            <div class="radio">
                                <label><input type="radio" name="available"
                                              value="yes">{{ trans('upload.batch.available.yes') }}</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="available"
                                              value="no">{{ trans('upload.batch.available.no') }}</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="available"
                                              value="contact">{{ trans('upload.batch.available.contact') }}</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="available"
                                              value="unknown">{{ trans('upload.batch.available.unknown') }}</label>
                            </div>
                        </div>
                    </form>
                    <div>
                        <button class="btn-primary btn" type="submit"
                                id="btnMassUpload"
                                form="massUploadForm">{{ trans('upload.batch.upload-button') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="creatorModal" role="dialog" data-url="{{url('/creators/create')}}"></div>
        <div class="modal fade" id="productModal" role="dialog" data-url="{{url('/products/create')}}"></div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#tags').tokenfield({
            autocomplete: {
                source: {!! $tags !!},
                delay: 300
            },
            showAutocompleteOnFocus: true
        });

        $('#creatorModalBtn').on('click', function (e) {
            var modal = $('#creatorModal');
            var link = modal.data('url');
            modal.load(link);
            modal.modal();
        });
        $('#productModalBtn').on('click', function (e) {
            var modal = $('#productModal');
            var link = modal.data('url');
            modal.load(link);
            modal.modal();
        });

        $('#btnMassUpload').on('click', function () {
            sendDataUpdate('massUploadForm');
        });
    </script>
@append