@extends('master')

@section('title')
    {{$metadata['name']}}
@endsection


@section('navigation')
    @include('partials.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('upload.individual.asset.title') }}</div>
                <div class="panel-body">
                    <div class="alert alert-info" role="alert">
                        @if (count($queue) <= 1)
                            {{ trans('upload.individual.queue.empty') }}
                        @elseif(count($queue) == 2)
                            {{ trans('upload.individual.queue.single', ['queueCount' => count($queue) - 1]) }}
                        @else
                            {{ trans('upload.individual.queue.multiple', ['queueCount' => count($queue) - 1]) }}
                        @endif
                    </div>
                    <form id="individualUploadForm" data-method="post"
                          data-url="{{ url('/upload/individual')}}"
                          action="javascript:void(0);">
                        {!! csrf_field() !!}
                        <div class="form-group" id="product_id-input">
                            <label class="control-label"
                                   for="product_id">{{ trans('upload.individual.asset.products') }}</label>
                            <div id="product_id-errors"></div>
                            <div class="input-group">
                                <select id="product_id" name="product_id" class="form-control">
                                    <option></option>
                                    @foreach($products as $product)
                                        @if(isset($batch['product']) && $product->id == $batch['product']->id)
                                            <option value="{{$product->id}}" selected>{{$product->name}}</option>
                                        @else
                                            <option value="{{$product->id}}">{{$product->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <div class="input-group-btn">
                                    <button class="btn btn-default"
                                            type="button"
                                            id="productModalBtn">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="creator_id-input">
                            <label class="control-label"
                                   for="creator_id">{{ trans('upload.individual.asset.creator') }}</label>
                            <div id="creator_id-errors"></div>
                            <div class="input-group">
                                <select id="creator_id" name="creator_id" class="form-control">
                                    <option></option>
                                    @foreach($creators as $creator)
                                        @if(isset($batch['creator']) && $creator->id == $batch['creator']->id)
                                            <option value="{{$creator->id}}"
                                                    selected>{{$creator->getFullName()}}</option>
                                        @else
                                            <option value="{{$creator->id}}">{{$creator->getFullName()}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <div class="input-group-btn">
                                    <button class="btn btn-default"
                                            type="button"
                                            id="creatorModalBtn">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <input hidden class="form-control" id="name" name="original_name" type="hidden"
                               value="{{$metadata['name']}}">
                        <div class="form-group" id="name-input">
                            <label class="control-label"
                                   for="name">{{ trans('upload.individual.asset.asset-name') }}</label>
                            <div id="name-errors"></div>
                            <input class="form-control" id="name" name="name" type="text"
                                   value="{{$metadata['name']}}">
                        </div>
                        <div class="form-group" id="description-input">
                            <label class="control-label"
                                   for="description">{{ trans('upload.individual.asset.description') }}</label>
                            <div id="description-errors"></div>
                            <textarea class="form-control" id="description" name="description"
                                      rows="5"></textarea>
                        </div>
                        <div class="form-group" id="tags-input">
                            <label class="control-label"
                                   for="tags">{{ trans('upload.individual.asset.tags') }}</label>
                            <div id="tags-errors"></div>
                            <input class="form-control" id="tags" name="tags" data-role="tagsinput"
                                   value="{{$batch['tags'] or null}}">
                        </div>
                        <div class="form-group" id="available-input">
                            <label class="control-label"
                                   for="available">{{ trans('upload.individual.asset.available.title') }}</label>
                            <div id="available-errors"></div>
                            <div class="radio">
                                <label><input type="radio" id="available-yes" name="available"
                                              value="yes">{{ trans('upload.individual.asset.available.yes') }}
                                </label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" id="available-no" name="available"
                                              value="no">{{ trans('upload.individual.asset.available.no') }}
                                </label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" id="available-contact" name="available"
                                              value="contact">{{ trans('upload.individual.asset.available.contact') }}
                                </label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" id="available-unknown" name="available"
                                              value="unknown">{{ trans('upload.individual.asset.available.unknown') }}
                                </label>
                            </div>
                        </div>
                    </form>
                    <div>
                        <button class="btn-primary btn" type="submit"
                                id="btnIndividualUpload"
                                form="individualUploadForm">{{ trans('upload.individual.upload-button') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('upload.individual.specifications.panel') }}</div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <img src="{{url('storage/processing/' . $metadata['name'] . '.'. $metadata['extension'])}}"
                             class="img-responsive" alt="{{$metadata['name']}}">
                    </div>
                    <div class="col-md-6">
                        <h4>{{ trans('upload.individual.specifications.title') }}</h4>
                        <p>{{ trans('upload.individual.specifications.size', ['size' => $image->filesize()]) }}</p>
                        <p>{{ trans('upload.individual.specifications.height', ['height' => $image->height()]) }}</p>
                        <p>{{ trans('upload.individual.specifications.width', ['width' => $image->width()]) }}</p>
                        <p>{{ trans('upload.individual.specifications.dimensions', ['height' => $image->height(), 'width' => $image->width()]) }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="creatorModal" role="dialog" data-url="{{url('/creators/create')}}"></div>
    <div class="modal fade" id="productModal" role="dialog" data-url="{{url('/products/create')}}"></div>
@endsection
@section('scripts')
    <script>
        $('#tags').tokenfield({
            autocomplete: {
                source: {!! $tags !!},
                delay: 300
            },
            showAutocompleteOnFocus: true
        });

        $('#creatorModalBtn').on('click', function (e) {
            var modal = $('#creatorModal');
            var link = modal.data('url');
            modal.load(link);
            modal.modal();
        });

        $('#productModalBtn').on('click', function (e) {
            var modal = $('#productModal');
            var link = modal.data('url');
            modal.load(link);
            modal.modal();
        });

        $('#btnIndividualUpload').on('click', function () {
            sendDataUpdate('individualUploadForm');
        });

        $(document).ready(function () {
            var availableId = 'available-' + '{!! $batch['available'] or null !!}';
            $("#" + availableId).prop("checked", true);
        });
    </script>
@append