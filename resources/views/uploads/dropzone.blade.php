@extends('master')

@section('title')
    {{ trans('upload.dropzone.page-title') }}
@endsection

@section('navigation')
    @include('partials.navbar')
@endsection

@section('content')
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <strong>{{trans('upload.dropzone.warning')}}</strong>
                <ul>
                    <li>{{trans('upload.dropzone.warning-size')}}</li>
                    <li>{{trans('upload.dropzone.warning-number')}}</li>
                </ul>

            </div>
            <div class="form-group">
                <form class="dropzone dz-area" id="upload" data-ajax="true">
                    {!! csrf_field() !!}
                </form>
            </div>
            <div class="form-group">
                <button class="btn btn-primary pull-right btn-lg disabled" type="button"
                        id="btnUpload">{{ trans('upload.dropzone.upload-button') }}</button>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">

        /* TODO: Set accepted files as per configuration */
        Dropzone.autoDiscover = false;
        var uploadDropzone = new Dropzone('#upload', {
            url: "{!!  url('upload')!!}",
            paramName: "assets",
            maxFiles: 20,
            maxFileSize: 20,
            maxThumbnailFilesize: 10,
            parallelUploads: 20,
            uploadMultiple: true,
            autoProcessQueue: false,
            addRemoveLinks: true,
            acceptedFiles: "{!! $allowedTypes !!}",
            dictDefaultMessage: '{{ trans("upload.dropzone.messages.default") }}',
            dictRemoveFile: '{{ trans("upload.dropzone.messages.remove-file") }}',
            dictInvalidFileType: '{{ trans("upload.dropzone.messages.invalid-type") }}',
            init: function(){
                this.on('addedfile', function () {
                    $('#btnUpload').toggleClass("disabled", false);
                });
                this.on('success', function (files, response) {
                    if (response.success) {
                        window.location = response.redirect;
                    }
                });
            }
        });

        $('#btnUpload').on('click', function () {
            uploadDropzone.processQueue();
        });
    </script>
@endsection