@extends('master')
@inject('converter', 'App\Services\ViewDataConversionService')

@section('title')
    {{ trans('assets.asset.page-title', ['assetName' => $asset->name]) }}
@endsection


@section('navigation')
    @include('partials.navbar')
@endsection

@section('content')
    <div class="col-md-4">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <div class="pull-left">{{$asset->name}}</div>
                    <a href="{{url($asset->getOriginalLocation())}}" type="button"
                       class="btn btn-default btn-xs pull-right" download="{{$asset->name . '.' . $asset->extension}}">
                        <span class="glyphicon glyphicon-download-alt"></span> {{ trans('assets.asset.image.downloadbtn')}}
                    </a>
                </div>
                <div class="panel-body">
                    <img class="img-responsive center-block" src="{{url($asset->getWebLocation())}}"
                         alt="{{$asset->name}}">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('assets.asset.overview.panel-title')}}
                    @if(Auth::user()->role === 'admin')
                        <button type="button"
                                class="btn btn-default btn-xs pull-right"
                                data-target="#assetDeleteModal"
                                data-url="{{url('/assets/'. $asset->id . '/delete')}}">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>
                    @endif
                    <button type="button"
                            class="btn btn-default btn-xs pull-right"
                            data-target="#assetUpdateModal"
                            data-url="{{url('/assets/'. $asset->id . '/edit')}}">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </button>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>{{ trans('assets.asset.overview.metadata.title')}}</h4>
                            <p>{{ trans('assets.asset.overview.metadata.name', ['name' => $asset->name])}}</p>
                            <p>{{ trans('assets.asset.overview.metadata.user', ['first_name' => $asset->user->first_name, 'last_name' => $asset->user->last_name])}}</p>
                            <p>{{ trans('assets.asset.overview.metadata.created', ['created_at' => $asset->created_at])}}</p>
                            @if($asset->created_at != $asset->updated_at)
                                <p>{{trans('assets.asset.overview.metadata.updated', ['updated_at' => $asset->updated_at])}}</p>
                        @endif
                        <!--<p>{{ trans('assets.asset.overview.metadata.original-name', ['original-name' => $asset->original_name])}}</p>-->
                            <p>{{ trans('assets.asset.overview.metadata.description', ['description' => $asset->description])}}</p>

                        </div>
                        <div class="col-md-6">
                            <h4>{{ trans('assets.asset.overview.specifications.title')}}</h4>
                            <p>{{ trans('assets.asset.overview.specifications.extension', ['extension' => $asset->extension])}}</p>
                            <p>{{ trans('assets.asset.overview.specifications.size', ['size' => $converter->formatBytes($asset->size, 0)]) }}</p>
                            <p>{{ trans('assets.asset.overview.specifications.dimensions', ['height' => $asset->assetable->height, 'width' =>$asset->assetable->width])}}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        @if($asset->product != null)
                            <div class="col-md-6">
                                <h4>{{ trans('assets.asset.overview.productdata.title')}}</h4>
                                <p>{{ trans('assets.asset.overview.productdata.name', ['product_name' => $asset->product->name])}}</p>
                            </div>
                        @endif
                        <div class="col-md-6">
                            <h4>{{ trans('assets.asset.overview.availability.title')}}</h4>
                            <p>{{ trans('assets.asset.overview.availability.available', ['available' => $asset->available])}}</p>

                        </div>
                    </div>
                    <hr>
                    @if($asset->creator != null)
                        <div class="row">
                            <div class="col-md-6">
                                <h4>{{ trans('assets.asset.overview.creator.title')}}</h4>
                                <p>{{ trans('assets.asset.overview.creator.name', ['first_name' => $asset->creator->first_name, 'last_name' => $asset->creator->last_name ])}}</p>
                                <p>{{ trans('assets.asset.overview.creator.function', ['function' => $asset->creator->function])}}</p>
                                <p>{{ trans('assets.asset.overview.creator.email', ['email' => $asset->creator->email])}}</p>
                            </div>
                        </div>
                        <hr>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <h4>{{ trans('assets.asset.overview.tags.title')}}</h4>
                            @foreach($asset->tags as $tag)
                                <a href="{{url('admin/tags')}}" class="btn btn-default">{{$tag->name}}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="assetUpdateModal" role="dialog"></div>
    <div class="modal fade" id="assetDeleteModal" role="dialog"></div>
@endsection
@section('scripts')
    <script>
        $("button[data-target$='Modal']").on('click', function (e) {
            var modal = $($(this).data('target'));
            var link = $(this).data('url');
            modal.load(link);
            modal.modal();
        });
    </script>
@append