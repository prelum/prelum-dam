@extends('master')

@section('title')
    {{ trans('assets.index.page-title') }}
@endsection

@section('navigation')
    @include('partials.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('assets.index.filter.title') }}</div>
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <form id="assetSearch" class="form-horizontal" onsubmit="event.preventDefault();">
                            <input hidden type="text" value="1" name="page">
                            <div class="input-group filter-custom-search-top">
                                <div class="input-group-addon">{{ trans('assets.index.filter.search-text') }}</div>
                                <input id="search" type="text" class="form-control"
                                       placeholder="{{ trans('assets.index.filter.search-placeholder') }}"
                                       name="filters[search]">
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" aria-expanded="true"
                                           href="#collapse1">{{ trans('assets.index.filter.type.title') }}</a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="checkbox clickable">
                                            <label>
                                                <input checked type="checkbox" value="image" name="filters[type][]">
                                                {{ trans('assets.index.filter.type.image') }}
                                            </label>
                                        </div>
                                    <!--<div class="checkbox clickable">
                                            <label>
                                                <input checked type="checkbox" value="video" name="filters[type][]">
                                                {{ trans('assets.index.filter.type.video') }}
                                            </label>
                                        </div>
                                        <div class="checkbox clickable">
                                            <label>
                                                <input checked type="checkbox" value="audio" name="filters[type][]">
                                                {{ trans('assets.index.filter.type.audio') }}
                                            </label>
                                        </div>
                                        <div class="checkbox clickable">
                                            <label>
                                                <input checked type="checkbox" value="document" name="filters[type][]">
                                                {{ trans('assets.index.filter.type.document') }}
                                            </label>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion"
                                           href="#collapse2">{{ trans('assets.index.filter.availability.title') }}</a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="checkbox clickable">
                                            <label>
                                                <input checked type="checkbox" value="yes" name="filters[available][]">
                                                {{ trans('assets.index.filter.availability.yes') }}
                                            </label>
                                        </div>
                                        <div class="checkbox clickable">
                                            <label>
                                                <input checked type="checkbox" value="no" name="filters[available][]">
                                                {{ trans('assets.index.filter.availability.no') }}
                                            </label>
                                        </div>
                                        <div class="checkbox clickable">
                                            <label>
                                                <input checked type="checkbox" value="contact"
                                                       name="filters[available][]">
                                                {{ trans('assets.index.filter.availability.contact') }}
                                            </label>
                                        </div>
                                        <div class="checkbox clickable">
                                            <label>
                                                <input checked type="checkbox" value="unknown"
                                                       name="filters[available][]">
                                                {{ trans('assets.index.filter.availability.unknown') }}
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion"
                                           href="#collapse3">{{ trans('assets.index.filter.size.title') }}</a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body ">
                                        <div class="clickable" id="filesize">
                                            <input hidden id="min"
                                                   type="text" value="{{ $filesize['min']}}"
                                                   name="filters[filesize][min]">
                                            <input hidden
                                                   id="max"
                                                   type="text" value="{{ $filesize['max']}}"
                                                   name="filters[filesize][max]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion"
                                           href="#collapse4">{{ trans('assets.index.filter.products') }}</a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="checkbox">
                                            <label>
                                                <input class="clickable" checked type="checkbox"
                                                       value="empty"
                                                       name="filters[product_id][]">
                                                Geen of onbekend
                                            </label>
                                        </div>
                                        @foreach($products as $product)
                                            <div class="checkbox">
                                                <label>
                                                    <input class="clickable" checked type="checkbox"
                                                           value="{{$product->id}}"
                                                           name="filters[product_id][]">
                                                    {{$product->name}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion"
                                           href="#collapse5">{{ trans('assets.index.filter.creators') }}</a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="checkbox">
                                            <label>
                                                <input class="clickable" checked type="checkbox"
                                                       value="empty"
                                                       name="filters[creator_id][]">
                                                Geen of onbekend
                                            </label>
                                        </div>
                                        @foreach($creators as $creator)
                                            <div class="checkbox">
                                                <label>
                                                    <input class="clickable" checked type="checkbox"
                                                           value="{{$creator->id}}"
                                                           name="filters[creator_id][]">
                                                    {{$creator->first_name}} {{$creator->last_name}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-group filter-custom-search-bottom">
                                <div class="col-lg-12">
                                    <button id="filter-submit" type="submit"
                                            class="btn btn-block btn-primary">{{ trans('assets.index.filter.search-button') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-10">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <form id="assetSet" class="form-inline" role="form">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="ordering">{{trans('assets.index.ordering.name')}}</label>
                            <select id="ordering" class="form-control" name="ordering">
                                @foreach($ordering['options'] as $key => $option)
                                    @if($key == $ordering['default'])
                                        <option value="{{$key}}"
                                                selected>{{trans('assets.index.ordering.' . $key)}}</option>
                                    @else
                                        <option value="{{$key}}">{{trans('assets.index.ordering.' . $key)}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group pull-right">
                            <label for="set-size">{{trans('assets.index.set-size.name')}}</label>
                            <select id="set-size" class="form-control" name="pagination">
                                @foreach($pagination['options'] as $option)
                                    @if($option == $pagination['default'])
                                        <option value="{{$option}}" selected>{{$option}}</option>
                                    @else
                                        <option value="{{$option}}">{{$option}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="panel-body">
                    <div id="assetView"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $(document).ready(function () {
                submitFilters();
            });

            var filesize = document.getElementById('filesize');
            var min = $('input[name="filters[filesize][min]"]').val();
            var max = $('input[name="filters[filesize][max]"]').val();
            noUiSlider.create(filesize, {
                start: [{!! $filesize['min'] -100 !!} , {!! $filesize['max']+100 !!} ],
                margin: 1000,
                range: {min: {!! $filesize['min'] !!}, max: {!! $filesize['max'] !!} },
                tooltips: [true, true],
                format: {
                    to: function (value) {
                        return formatBytes(value, 0);
                    },
                    from: function (value) {
                        return value;
                    }
                }

            });
            filesize.noUiSlider.on('change', function (values, handle, unencodedValues) {
                $('input[name="filters[filesize][min]"]').val(unencodedValues[0]);
                $('input[name="filters[filesize][max]"]').val(unencodedValues[1]);
            });

            $("#assetSet").on('change', function (e) {
                $.ajax({
                    type: "POST",
                    url: '{{url('/assets/resultsettings')}}',
                    data: $("#assetSet").serialize(),
                    success: function (data) {
                        submitFilters();
                    }
                });
                e.preventDefault();
            });

            $(document).on('click', '.pagination a', function (link) {
                link.preventDefault();
                getFilteredAssets($(this).attr('href'));
            });

            $('#filter-submit').on('click', function () {
                submitFilters();
            });

            function getFilteredAssets(url) {
                $.ajax(url, {
                    dataType: "html",
                    async: false,
                    success: function (data) {
                        $('#assetView').html(data);
                    }
                });
            }

            function submitFilters() {
                $("#assetView").addClass('overlay');
                var formVars = $('#assetSearch').serialize();
                getFilteredAssets("{{url('/assets/filter')}}?" + formVars);
                $("#assetView").removeClass('overlay');
            }
        });
    </script>
@append