<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <link media="all" rel="stylesheet" type="text/css" href="{{ asset('css/vendor.css') }}">
    <link media="all" rel="stylesheet" type="text/css" href="{{ asset('css/all.css') }}">
    @yield('links')

</head>
<body>
<div id="wrapper">
    @yield('navigation')
    <div id="page-wrapper">
        <div class="container-fluid">
            @yield('content')
        </div>
    </div>
</div>
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/all.js') }}"></script>
@yield('scripts')
</body>
</html>