@extends('master')

@section('title')
    {{ trans('admin.users.page-title') }}
@endsection

@section('navigation')
    @include('partials.navbar')
@endsection

@section('content')
    <div class="col-md-12">
        <div class="panel panel-default clearfix">
            <div class="panel-heading">
                {{trans('admin.users.overview.title')}}
                <button class="btn btn-default btn-xs pull-right"
                        type="button"
                        data-target="#userCreateModal"
                        data-url="{{url('users/create')}}">
                    <span class="glyphicon glyphicon-plus"></span></button>
            </div>
            <div class="panel-body">
                <table id="users" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>{{trans('admin.users.overview.id')}}</th>
                        <th>{{trans('admin.users.overview.first-name')}}</th>
                        <th>{{trans('admin.users.overview.last-name')}}</th>
                        <th>{{trans('admin.users.overview.email')}}</th>
                        <th>{{trans('admin.users.overview.role')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->first_name}}</td>
                            <td>{{$user->last_name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->roles->first()->name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="userCreateModal" role="dialog"></div>
    <div class="modal fade" id="userUpdateModal" role="dialog"></div>
@endsection

@section('scripts')
    <script>
        var usersTable = $('#users').DataTable({
            "columnDefs": [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                }
            ]
        });
        $('#users tbody').on('dblclick', 'tr', function () {
            var data = usersTable.row(this).data();
            var modal = $('#userUpdateModal');
            link = '{!! url('users') !!}/' + data[0] + '/edit';
            modal.load(link);
            modal.modal();
        });
        $("button[data-target$='Modal']").on('click', function (e) {
            var modal = $($(this).data('target'));
            var link = $(this).data('url');
            modal.load(link);
            modal.modal();
        });
    </script>
@append