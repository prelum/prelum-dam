@extends('master')

@section('title')
    {{ trans('admin.tags.page-title') }}
@endsection

@section('navigation')
    @include('partials.navbar')
@endsection

@section('content')
    <div class="col-md-12">
        <div class="panel panel-default clearfix">
            <div class="panel-heading">
                {{trans('admin.tags.overview.title')}}
            </div>
            <div class="panel-body">
                <table id="tags" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>{{trans('admin.tags.overview.id')}}</th>
                        <th>{{trans('admin.tags.overview.name')}}</th>
                        <th>{{trans('admin.tags.overview.count')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tags as $tag)
                        <tr>
                            <td>{{$tag->id}}</td>
                            <td>{{$tag->name}}</td>
                            <td>{{$tag->count}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var usersTable = $('#tags').DataTable({
            "columnDefs": [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                }
            ]
        });
    </script>
@append