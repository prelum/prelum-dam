@extends('master')

@section('title')
    {{ trans('admin.products.page-title') }}
@endsection

@section('navigation')
    @include('partials.navbar')
@endsection

@section('content')
    <div class="col-md-12">
        <div class="panel panel-default clearfix">
            <div class="panel-heading">
                {{trans('admin.products.overview.title')}}
                <button class="btn btn-default btn-xs pull-right"
                        type="button"
                        data-target="#productCreateModal"
                        data-url="{{url('products/create')}}">
                    <span class="glyphicon glyphicon-plus"></span></button>
            </div>
            <div class="panel-body">
                <table id="products" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>{{trans('admin.products.overview.id')}}</th>
                        <th>{{trans('admin.products.overview.productcode')}}</th>
                        <th>{{trans('admin.products.overview.name')}}</th>
                        <th>{{trans('admin.products.overview.website')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->productCode}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->website}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="productCreateModal" role="dialog"></div>
    <div class="modal fade" id="productUpdateModal" role="dialog"></div>
@endsection

@section('scripts')
    <script>
        var productsTable = $('#products').DataTable({
            "columnDefs": [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                }
            ]
        });
        $('#products tbody').on('dblclick', 'tr', function () {
            var data = productsTable.row(this).data();
            var modal = $('#productUpdateModal');
            link = '{!! url('products') !!}/' + data[0] + '/edit';
            modal.load(link);
            modal.modal();
        });
        $("button[data-target$='Modal']").on('click', function (e) {
            var modal = $($(this).data('target'));
            var link = $(this).data('url');
            modal.load(link);
            modal.modal();
        });
    </script>
@append