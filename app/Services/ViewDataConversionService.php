<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 16-3-2017
 * Time: 16:06
 */

namespace App\Services;

/**
 * Class ViewDataConversionService
 * @package App\Services
 *
 * Service om verschillende data-elementen te kunnen converteren in Blade
 */
class ViewDataConversionService{
    /**
     * @param $bytes
     * @param int $precision
     * @return string
     *
     * Formateert bytes naar een meer leesbare variant
     */
    public function formatBytes($bytes, $precision = 2){
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
}