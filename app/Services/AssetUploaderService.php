<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 4-3-2016
 * Time: 11:44
 */

namespace App\Services;

use App\Libraries\Upload\UploaderFactory;
use App\Models\Asset\ImageAsset;

/**
 * Class AssetUploaderService
 * @package App\Services
 *
 * Genereert een uploader om de asset toe te voegen
 */
class AssetUploaderService{

    protected $uploaderFactory;

    public function __construct(UploaderFactory $uploaderFactory){
        $this->uploaderFactory = $uploaderFactory;
    }

    /**
     * @param $data
     *
     * Genereert een uploader en voert de uploadactie uit
     */
    public function uploadAsset($data){
        $uploader = $this->uploaderFactory->createUploader($data);
        $uploader->upload();
    }


}