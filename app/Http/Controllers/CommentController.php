<?php

namespace App\Http\Controllers;

use App\Models\BaseAsset;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($asset_id){
        return view('modals.comment', ['asset_id' => $asset_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $asset_id){
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'text' => 'required',
        ]);

        if($validator->fails()){
            return response($validator->errors()->toArray(), 422);
        }

        try{
            $asset = BaseAsset::find($asset_id);
            $user = Auth::user();
            $comment = new Comment();
            $comment->title = $request->input('title');
            $comment->text = $request->input('text');
            $asset->comments()->save($comment);
            $user->comments()->save($comment);
            return response($comment, 201);
        }catch(\Exception $e){
            return response([$e->getCode(), $e->getMessage()], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($asset_id, $comment_id){
        $comment = Comment::find($comment_id);
        return view('modals.comment', ['comment' => $comment, 'asset_id' => $asset_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'text' => 'required',
        ]);

        if($validator->fails()){
            return response($validator->errors()->toArray(), 422);
        }
        $comment = Comment::find($id);
        $comment->title = $request->input('title');
        $comment->text = $request->input('text');
        $comment->save();
        return response($comment, 201);
    }

    public function delete($asset_id, $comment_id){
        return view('modals.delete', ['url' => url('assets/' . $asset_id . '/comments/' . $comment_id)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $comment_id){
        $comment = Comment::find($comment_id);
        $comment->delete();
        return response(['redirect' => url('/assets/' . $id)], 202);
    }
}
