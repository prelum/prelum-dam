<?php

namespace App\Http\Controllers;

use App\Models\Asset\Asset;
use App\Models\Creator;
use App\Models\Product;
use App\Repositories\AssetRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class AssetController extends Controller{

    protected $assetRepository;

    /**
     * AssetController constructor.
     * @param AssetRepository $assetRepository
     */
    public function __construct(AssetRepository $assetRepository){
        $this->assetRepository = $assetRepository;
    }

    /**
     * @param Request $request
     * @return View
     *
     * Toont de overzichtspagina van assets
     */
    public function index(Request $request){
        if(!Session::has('view')){
            Session::put('view', ['pagination' => Config::get('dam.pagination.default'), 'ordering' => Config::get('dam.ordering.default')]);
        }
        return view('assets.index', ['pagination' => Config::get('dam.pagination'), 'ordering' => Config::get('dam.ordering'), 'products' => Product::all(), 'creators' => Creator::all(), 'filesize' => $this->assetRepository->getFileSizeRange()]);
    }

    /**
     * @param Request $request
     * @return mixed
     *
     * Filtert de assets op basis van requests en geeft de bijbehorende view terug
     */
    public function filter(Request $request){
        $filters = $request->get('filters');
        $filteredAssets = $this->assetRepository->filter($filters, Session::get('view.ordering'));
        $paginatedAssets = $filteredAssets->paginate(Session::get('view.pagination'))->appends(['filters' => $filters]);
        return View::make('partials.assetview', array('assets' => $paginatedAssets, 'assetsPerRow' => Config::get('dam.pagination.assets-per-row')))->render();
    }

    /**
     * @param Request $request
     *
     * Past de ordering van de assets aan
     */
    public function changeResultSet(Request $request){
        Session::put('view.ordering', $request->get('ordering'));
        Session::put('view.pagination', $request->get('pagination'));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     *
     * Downloaden van asset
     */
    public function download($id){
        $asset = Asset::find($id);
        return response()->download(storage_path($asset->getAssetFileLocation() . '/' . $asset->id . '.original.' . $asset->extension), $asset->name);
    }

    /**
     * @return View
     *
     * Toont het creatie-formulier van de asset (in modal-vorm)
     */
    public function create(){
        return view('modals.asset');
    }

    /**
     * @param $id
     * @return View
     *
     * Toont de detailpagina van de asset
     */
    public function show($id){
        $asset = Asset::find($id);
        $products = Product::all();
        $creators = Creator::all();
        $tags = json_encode(DB::table('tagging_tags')->pluck('name'));
        return view('assets.asset', ['asset' => $asset, 'products' => $products, 'creators' => $creators, 'tags' => $tags]);
    }

    /**
     * @param $id
     * @return View
     *
     * Toont het update-formulier van de asset
     */
    public function edit($id){
        $asset = Asset::find($id);
        $products = Product::all();
        $creators = Creator::all();
        $tags = DB::table('tagging_tags')->pluck('name');
        $availabilityTypes = $asset->getEnumerablePossibilities('available');
        return view('modals.asset', ['asset' => $asset, 'products' => $products, 'creators' => $creators, 'tags' => json_encode($tags), 'available' => $availabilityTypes]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * Update de asset meta-data
     */
    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'product_id' => 'required:numeric',
            'creator_id' => 'numeric',
            'name' => 'required|max:255',
            'description' => 'required',
            'available' => 'required|in:yes,no,contact,unknown'
        ]);

        if($validator->fails()){
            return response($validator->errors()->toArray(), 422);
        }

        $asset = Asset::find($id);
        $data = $request->only(['name', 'description', 'available']);
        $asset->update($data);

        $product = Product::find($request->input('product_id'));
        $product->assets()->save($asset);

        $creator = Creator::find($request->input('creator_id'));
        if($creator == null){
            $asset->creator()->dissociate();
        }else{
            $creator->assets()->save($asset);
        }

        $tagstring = $request->input('tags');
        if($tagstring != ''){
            $tags = explode(', ', $tagstring);
            $asset->retag($tags);
        }

        $asset->save();
        return response($asset, 201);
    }

    /**
     * @param $id
     * @return View
     *
     * Delete-functie (niet geimplementeerd)
     */
    public function delete($id){
        return view('modals.delete', ['url' => url('assets/' . $id)]);
    }

    /**
     * TODO: Ensure all tags and subdata is deleted
     *
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id){
        $asset = Asset::find($id);
        $asset->delete();
        return response(['redirect' => url('/')], 202);
    }

}
