<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class UserController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $roles = Role::all()->pluck('name');
        return view('modals.user', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'role' => 'required|exists:roles,name',
            'password-repeat' => 'required|same:password'
        ]);

        if($validator->fails()){
            return response($validator->errors()->toArray(), 422);
        }

        $data = $request->only(['first_name', 'last_name', 'email']);
        $user = new User($data);
        $user->password = Hash::make($request->get('password'));
        $user->save();
        $user->assignRole($request->get('role'));
        return response([$user], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $user = Auth::user();
        return view('users.user', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $roles = Role::all()->pluck('name');
        $user = User::find($id);
        return view('modals.user', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
            'role' => 'required|in:admin,user',
            'password-repeat' => 'required|same:password'
        ]);

        if($validator->fails()){
            return response($validator->errors()->toArray(), 422);
        }

        $data = $request->only(['first_name', 'last_name', 'email']);
        $user = User::find($id);
        $user->update($data);
        $user->password = Hash::make($request->get('password'));
        $user->save();
        $user->syncRoles($request->get('role'));
        return response([$user], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //
    }
}
