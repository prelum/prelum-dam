<?php

namespace App\Http\Controllers\Uploads;

use App\Services\AssetUploaderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\BaseAsset;
use App\Models\ImageAsset;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Creator;

class IndividualUploadsController extends Controller{
    protected $assetUploaderService;

    public function __construct(AssetUploaderService $assetUploaderService){
        $this->assetUploaderService = $assetUploaderService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(){
        $upload = Session::get('upload');
        $assetKey = key($upload['queue']);
        $image = Image::make(storage_path('app/public/processing/' . $assetKey));
        $products = Product::all();
        $creators = Creator::all();
        return view('uploads.individual', array('metadata' => $upload['queue'][$assetKey], 'queue' => $upload['queue'], 'batch' => $upload['batch'], 'image' => $image, 'tags' => json_encode(DB::table('tagging_tags')->pluck('name')), 'products' => $products, 'creators' => $creators));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * Slaat de individuele asset op, verwijst naar de volgende wanneer meerdere assets in de queue staan, of redirect naar home wanneer deze leeg is
     */
    public function store(Request $request){
        $queue = Session::get('upload.queue');
        $key = key($queue);
        $validator = Validator::make($request->all(), [
            'product_id' => 'numeric',
            'creator_id' => 'numeric',
            'name' => 'required|max:255',
            'description' => 'required',
            'available' => 'required|in:yes,no,contact,unknown'
        ]);

        if($validator->fails()){
            return response($validator->errors()->toArray(), 422);
        }
        $data = $request->only(['product_id', 'creator_id', 'original_name', 'name', 'description', 'tags', 'available']);
        $mergedData = array_merge($queue[$key], $data);
        $this->assetUploaderService->uploadAsset($mergedData);

        unset($queue[$key]);
        if(!empty($queue)){
            Session::put('upload.queue', $queue);
            return response(['redirect' => url('/upload/individual/' . key($queue))], 201);
        }else{
            $request->session()->forget('upload');
            return response(['redirect' => url('/')], 201);
        }
    }
}
