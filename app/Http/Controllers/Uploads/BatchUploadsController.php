<?php

namespace App\Http\Controllers\Uploads;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Models\Creator;

class BatchUploadsController extends Controller
{

    public function index()
    {
        $products = Product::all();
        $creators = Creator::all();
        return view('uploads.batch', ['queue' => Session::get('upload.queue'), 'tags' => json_encode(DB::table('tagging_tags')->pluck('name')), 'products' => $products, 'creators' => $creators]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * Voegt batch data toe voor invulling per individuele asset, redirect naar de eerste individuele asset van de queue
     */
    public function batchUpload(Request $request)
    {
        $queue = Session::get('upload.queue');
        try {
            $batch['product'] = Product::find($request->input('product_id'));
            $batch['creator'] = Creator::find($request->input('creator_id'));
            $batch['available'] = $request->input('available');
            $batch['tags'] = $request->input('tags');
            Session::put('upload.batch', $batch);
        } catch(\Exception $e){
            return response($e, 422);
        }
        return response(['redirect' => url('/upload/individual/' . key($queue))], 201);
    }


}
