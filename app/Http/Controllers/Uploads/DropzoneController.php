<?php

namespace App\Http\Controllers\Uploads;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use App\Models\QueuedAsset;

class DropzoneController extends Controller{

    /**
     * Display page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $allowedTypes = '.' . implode(',.', array_keys(config('dam.allowed_types')));
        return view('uploads.dropzone', ['allowedTypes' => $allowedTypes]);
    }

    /**
     * Uploads files to processing storage. Redirects to batch or individual upload based on asset count
     *
     * @return \Illuminate\Http\Response
     */
    public function post(Request $request){
        $input = $request->assets;
        $queue = [];
        foreach($input as $asset){
            $queue[$asset->getClientOriginalName()] = [
                'name' => str_replace('.' . $asset->getClientOriginalExtension(), '', $asset->getClientOriginalName()),
                'mimetype' => $asset->getMimeType(),
                'extension' => $asset->getClientOriginalExtension(),
                'size' => $asset->getClientSize()
            ];
            $asset->move(storage_path('app/public/processing'), $asset->getClientOriginalName());
        }

        Session::put('upload.batch', []);
        Session::put('upload.queue', $queue);
        if(count($queue) > 1){
            return array('success' => true, 'redirect' => URL::to('/upload/batch'));
        }elseif(count($queue) == 1){
            return array('success' => true, 'redirect' => URL::to('/upload/individual/' . key($queue)));
        }
    }

}
