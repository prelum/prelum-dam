<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Asset\Asset;
use App\Models\Asset\ImageAsset;
use App\Models\Product;
use App\User;
use Illuminate\Support\Facades\URL;

class DashboardController extends Controller{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return redirect('assets');
    }
}