<?php

namespace App\Http\Controllers;

use App\Models\Creator;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CreatorController extends Controller{


    public function create(){
        return view('modals.creator');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'first-name' => 'required|max:255',
            'last-name' => 'required|max:255',
            'email' => 'email'
        ]);
        if($validator->fails()){
            return response($validator->errors()->toArray(), 422);
        }

        $user = Auth::user();
        $creator = new Creator();

        $creator->first_name = $request->get('first-name');
        $creator->last_name = $request->get('last-name');
        $creator->function = $request->get('function');
        $creator->email = $request->get('email');

        try{
            $user->addedCreators()->save($creator);
            $creator->save();
        }catch(QueryException $e){
            return response($e->getMessage(), 422);
        }
        return response($creator, 201);
    }

    public function edit($id){
        $creator = Creator::find($id);
        return view('modals.creator', ['creator' => $creator]);
    }

}
