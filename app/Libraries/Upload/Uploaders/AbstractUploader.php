<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 4/6/2016
 * Time: 10:13 PM
 */

namespace App\Libraries\Upload\Uploaders;

use App\Models\Creator;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

/**
 * Class AbstractUploader
 * @package App\Libraries\Upload\Uploaders
 *
 * Basisfuncties van de Uploader groep
 */
abstract class AbstractUploader
{
    protected $data;
    protected $processingLocation;
    protected $asset;

    public function __construct($data)
    {
        $this->data = $data;
        $this->processingLocation = 'public/processing/' . $this->data['original_name'] . '.' . $this->data['extension'];
    }

    /**
     * Voert de daadwerkelijke actie uit. Deze kan per Uploader natuurlijk overschreven worden
     */
    public function upload()
    {
        $this->saveData();
        $this->addUser();
        $this->addCreator();
        $this->addProduct();
        $this->addTags();
        $this->createStorageDirectory();
        $this->generateFiles();
    }

    /**
     * @return mixed
     *
     * functie om data te saven naar de database
     */
    abstract protected function saveData();

    /**
     * @return mixed
     *
     * Functie pointer voor de functie om de noodzakelijk systeem-files te genereren
     */
    abstract protected function generateFiles();

    /**
     * Genereert de noodzakelijke storage directory wanneer deze niet beschikbaar is
     */
    protected function createStorageDirectory()
    {
        if (!File::exists('public/'. $this->asset->getAssetLocation())) {
            Storage::makeDirectory('public/'.$this->asset->getAssetLocation(), 0777, true, true);
        }
    }

    /**
     * Voegt een creator toe aan de asset
     */
    protected function addCreator()
    {
        if (!empty($this->data['creator_id'])) {
            $creator = Creator::find($this->data['creator_id']);
            $creator->assets()->save($this->asset);
        }
    }

    /**
     * Voegt een product toe aan de asset
     */
    protected function addProduct()
    {
        if (!empty($this->data['product_id'])) {
            $product = Product::find($this->data['product_id']);
            $product->assets()->save($this->asset);
        }
    }

    /**
     * Voegt de invoerende gebruiker (owner) toe aan de asset
     */
    protected function addUser()
    {
        $user = Auth::user();
        $user->addedAssets()->save($this->asset);
    }

    /**
     * Voegt alle tags toe aan de asset
     */
    protected function addTags()
    {
        if (!empty($this->data['tags'])) {
            $tags = explode(', ', $this->data['tags']);
            foreach ($tags as $tag) {
                $this->asset->tag($tag);
            }
        }
    }
}