<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 4/6/2016
 * Time: 9:57 PM
 */

namespace App\Libraries\Upload\Uploaders;


use App\Libraries\Upload\Uploaders\AbstractUploader;
use App\Libraries\Upload\Uploaders\UploaderInterface;
use App\Models\Asset\Asset;
use App\Models\Asset\SubAsset\ImageAsset;
use Illuminate\Support\Facades\Storage;
use Imagick;

/**
 * Class ImageUploader
 * @package App\Libraries\Upload\Uploaders
 *
 * Uploader voor images
 */
class ImageUploader extends AbstractUploader implements UploaderInterface{
    protected $image;
    protected $imageAsset;

    /**
     * ImageUploader constructor.
     * @param $data
     *
     * Maakt een Imagick image aan van de file in de processing map
     */
    public function __construct($data){
        parent::__construct($data);
        $this->image = new Imagick(storage_path('app/' . $this->processingLocation));
        $this->data['type'] = 'image';
    }

    /**
     * Genereert een thumbail en een web-image van de image, en copiëert de originele file van processing naar storage
     */
    protected function generateFiles(){
        $thumbnail = $this->image;
        $thumbnail->thumbnailImage(config('dam.thumbnail.default-width'), 0);
        $thumbnail->writeImage(storage_path("app/public/" . $this->asset->getAssetLocation($this->asset->id . ".thumbnail." . config('dam.thumbnail.default-extension'))));

        $web = $this->image;
        $web->writeImage(storage_path("app/public/" . $this->asset->getAssetLocation($this->asset->id . ".web." . config('dam.web.default-extension'))));

        Storage::move($this->processingLocation, 'public/' . $this->asset->getAssetLocation($this->asset->id . ".original." . $this->asset->extension));
    }

    /**
     * @throws \Exception
     *
     * saves de data naar de database
     */
    protected function saveData(){
        try{
            $this->asset = new Asset($this->data);
            $this->asset->save();
            $this->imageAsset = new ImageAsset($this->data);
            $this->imageAsset->height = $this->image->getImageHeight();
            $this->imageAsset->width = $this->image->getImageWidth();
            $this->imageAsset->save();
            $this->imageAsset->asset()->save($this->asset);
        }catch(\Exception $e){
            throw $e;
        }
    }
}