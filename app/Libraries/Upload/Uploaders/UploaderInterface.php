<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 4/6/2016
 * Time: 9:58 PM
 */

namespace App\Libraries\Upload\Uploaders;

/**
 * Interface UploaderInterface
 * @package App\Libraries\Upload
 *
 */
interface UploaderInterface
{
    /**
     * UploaderInterface constructor.
     * @param $data
     */
    public function __construct($data);

    /**
     * @return mixed
     */
    public function upload();
}