<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 4/10/2016
 * Time: 2:32 PM
 */

namespace App\Libraries\Upload;

use App\Exceptions\UnknownTypeException;
use App\Libraries\Upload\Uploaders\DocumentUploader;
use App\Libraries\Upload\Uploaders\ImageUploader;
use App\Libraries\Upload\Uploaders\UploaderInterface;
use App\Libraries\Upload\Uploaders\VectorUploader;

class UploaderFactory
{
    /**
     * @param $data
     * @return UploaderInterface
     * @throws UnknownTypeException
     *
     * Genereert een Uploader op basis van de extensie. Zie de dam-configuratie voor de mogelijke extensies en hun conversiewaarde
     *
     * De Uploader moet de UploaderInterface implementeren
     */
    public function createUploader($data)
    {
        $type = null;
        $loweredExtension = strtolower($data['extension']);
        if (array_key_exists($loweredExtension, config('dam.allowed_types'))) {
            $type = config('dam.allowed_types')[$loweredExtension];
        } else {
            throw new UnknownTypeException($data['extension'] . " not found");
        }

        switch ($type) {
            case 'image':
                return new ImageUploader($data);
        }
    }
}