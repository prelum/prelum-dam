<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 2-2-2016
 * Time: 14:09
 */

namespace App\Repositories;

use App\Models\Asset\Asset;
use App\Models\TaggingTag;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


class AssetRepository{
    public static function getAllAssets(){
        return Asset::all();
    }

    public static function filter($filters, $ordering){
        $orderQuery = Config::get("dam.ordering.options.$ordering");
        $assets = Asset::where(function($query) use ($filters){
            if(array_key_exists('filesize', $filters)){
                $query->where('size', '>=', $filters['filesize']['min'])
                    ->where('size', '<=', $filters['filesize']['max']);
            }
            if(array_key_exists('search', $filters)){
                $parts = explode(' ', $filters['search']);
                $query->where(function($query) use ($parts){
                    foreach($parts as $part){
                        $query->orWhere('name', 'like', '%' . $part . '%');
                        $query->orWhere('description', 'like', '%' . $part . '%');
                        $query->orWhereHas('tagged', function($query) use ($part){
                            $query->where('tag_name', 'like', '%' . $part . '%');
                        });
                    }
                });
            }
            if(array_key_exists('type', $filters)){
                $typeFilters = $filters['type'];
                $query->where(function($query) use ($typeFilters){
                    foreach($typeFilters as $filterValue){
                        $query->orWhere('type', $filterValue);
                    }
                });
            }
            if(array_key_exists('available', $filters)){
                $typeFilters = $filters['available'];
                $query->where(function($query) use ($typeFilters){
                    foreach($typeFilters as $filterValue){
                        $query->orWhere('available', $filterValue);
                    }
                });
            }
            if(array_key_exists('product_id', $filters)){
                $typeFilters = $filters['product_id'];
                $query->where(function($query) use ($typeFilters){
                    foreach($typeFilters as $filterValue){
                        if($filterValue == 'empty'){
                            $query->orWhereNull('product_id');
                        }else{
                            $query->orWhere('product_id', $filterValue);
                        }
                    }
                });
            }

            if(array_key_exists('creator_id', $filters)){
                $typeFilters = $filters['creator_id'];
                $query->where(function($query) use ($typeFilters){
                    foreach($typeFilters as $filterValue){
                        if($filterValue == 'empty'){
                            $query->orWhereNull('creator_id');
                        }else{
                            $query->orWhere('creator_id', $filterValue);
                        }
                    }
                });
            }
        });
        return $assets->orderBy($orderQuery[0], $orderQuery[1]);
    }

    public static function getFileSizeRange(){
        return [
            'min' => DB::table('assets')->min('size') - 10,
            'max' => DB::table('assets')->max('size') + 10
        ];
    }
}