<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 */
class Log extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'owner_type',
        'owner_id',
        'old_value',
        'new_value',
        'type'
    ];

    protected $guarded = [];

        
}