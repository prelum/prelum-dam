<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\AuditingTrait;

/**
 * Class Comment
 */
class Comment extends Model
{

    use AuditingTrait;

    public $timestamps = true;
    protected $fillable = [
        'user_id',
        'asset_id',
        'comment'
    ];
    protected $guarded = [];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
