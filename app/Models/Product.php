<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 */
class Product extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'productCode',
        'name',
        'website'
    ];

    protected $guarded = [];

    public function assets()
    {
        return $this->hasMany('App\Models\Asset\Asset');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

        
}