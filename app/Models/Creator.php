<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Creator
 */
class Creator extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'function',
        'email'
    ];

    protected $guarded = [];

    public function assets()
    {
        return $this->hasMany('App\Models\Asset\Asset');
    }

    public function user(){
         return $this->hasOne('App\User');
    }

    public function getFullName()
    {
        return trim($this->first_name . ' ' . $this->last_name . ' - ' . $this->email);
    }


}