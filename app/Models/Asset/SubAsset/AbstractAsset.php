<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 6-4-2016
 * Time: 15:28
 */

namespace App\Models\Asset\SubAsset;

use App\Models\Asset\Asset;
use Illuminate\Database\Eloquent\Model;

class AbstractAsset extends Model
{
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function asset()
    {
        return $this->morphOne(Asset::class, 'assetable');
    }
}