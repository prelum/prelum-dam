<?php

namespace App\Models\Asset\SubAsset;

class ImageAsset extends AbstractAsset
{

    protected $table = "image_assets";
    public $timestamps = false;

    protected $fillable = [
        'height',
        'width'
    ];


}