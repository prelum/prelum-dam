<?php

namespace App\Models\Asset;

use App\Models\Creator;
use App\Models\Product;
use App\Models\User;
use App\Traits\Commentable;
use App\Traits\Enumerable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\AuditingTrait;
use Conner\Tagging\Taggable;

/**
 * Class Asset
 * @package App\Models\Asset
 *
 * Root model van een asset. Deze bevat alle data die gelijk is voor alle assets.
 *
 * Dit model is vervolgens te extenden door SubAssets toe te voegen met de data specifiek aan dat type.
 * Zie ook de Laravel documentatie over morph-relaties voor meer informatie hoe dit werkt
 */
class Asset extends Model{

    use AuditingTrait,
        Taggable,
        Commentable, Enumerable;

    protected $dontKeepLogOf = ['type', 'mimetype', 'extension', 'size', 'original_name', 'created_at', 'updated_at'];

    protected $fillable = [
        'type',
        'mimetype',
        'extension',
        'size',
        'original_name',
        'name',
        'description',
        'available'
    ];
    protected $guarded = [];

    public static $logCustomFields = [
        'creator' => [
            'created' => '{new.creator.first_name} {new.creator.last_name}',
            'updated' => '{new.creator.first_name} {new.creator.last_name}',
        ],
        'product' => [
            'created' => '{new.product.name}',
            'updated' => '{new.product.name}',
        ],
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function creator(){
        return $this->belongsTo(Creator::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function assetable(){
        return $this->morphTo();
    }

    public function stringifyTags(){
        $tags = array();
        foreach($this->tags as $tag){
            $tags[] = $tag->name;
        }
        return implode(',', $tags);
    }

    public function getAssetLocation($file = ""){
        $date = $this->created_at;
        return "$this->type/$date->year/$date->month/$date->day/$file";
    }

    public function getThumbnailLocation(){
        return "storage/" . $this->getAssetLocation("$this->id.thumbnail.". config('dam.thumbnail.default-extension'));
    }

    public function getOriginalLocation(){
        return "storage/" . $this->getAssetLocation("$this->id.original.$this->extension");
    }

    public function getWebLocation(){
        return "storage/" . $this->getAssetLocation("$this->id.web.". config('dam.web.default-extension'));
    }
}
