<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable{

    use Notifiable, HasRoles;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'role'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function addedAssets(){
        return $this->hasMany('App\Models\Asset\Asset');
    }

    public function addedProducts(){
        return $this->hasMany('App\Models\Product');
    }

    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }

    public function addedCreators(){
        return $this->hasMany('App\Models\Creator');
    }

}
