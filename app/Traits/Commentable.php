<?php

namespace App\Traits;

use App\Models\Comment;

/**
 * Class Commentable
 * @package App\Traits
 *
 * Trait om comments mogelijk te maken op een model.
 *
 * TODO: Dit wordt nog nergens daadwerkelijk geimplementeerd
 */
trait Commentable
{
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

}