<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 7-3-2016
 * Time: 11:51
 */

namespace App\Traits;

use App\Exceptions\UnknownEnumFieldException;
use Illuminate\Support\Facades\Config;

/**
 * Class Enumerable
 * @package App\Traits
 *
 * Simuleert een enum-veld. De opgegeven velden zijn te vinden in de configuratie
 */
trait Enumerable{

    public function getEnumerablePossibilities($column){
        if(!Config::has("dam.enum.$column")){
            throw new UnknownEnumFieldException();
        }
        return config("dam.enum.$column");
    }
}