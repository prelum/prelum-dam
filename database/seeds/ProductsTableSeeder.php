<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
            [
                'productCode' => 'AI',
                'user_id' => '1',
                'name' => 'A&I',
                'website' => 'www.a-en-i.nl',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ]);
        DB::table('products')->insert(
            [
                'productCode' => 'NTT',
                'user_id' => '1',
                'name' => 'NTvT',
                'website' => 'www.ntvt.nl',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ]
        );
    }
}
