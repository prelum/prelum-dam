<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'first_name' => 'Test',
                'last_name' => 'Admin',
                'email' => 'admin@prelum.nl',
                'password' => bcrypt('admin'),
                'verified' => true,
            ]);
        DB::table('users')->insert(
            [
                'first_name' => 'Test',
                'last_name' => 'User',
                'email' => 'user@prelum.nl',
                'password' => bcrypt('user'),
                'verified' => true,
            ]
        );
    }
}
