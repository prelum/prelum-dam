<?php

use Illuminate\Database\Seeder;

class AssetsSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $faker = Faker\Factory::create('nl_NL');
        $manager = new \Intervention\Image\ImageManager(array('driver' => 'imagick'));

        for($i = 1; $i <= 100; $i++){
            $time = \Carbon\Carbon::now();
            $extension = $faker->randomElement(['jpg', 'png']);
            $location = 'public/image/' . $time->year . '/' . $time->month . '/' . $time->day . '/';
            if(!\Illuminate\Support\Facades\File::exists($location)){
                \Illuminate\Support\Facades\Storage::makeDirectory($location, 0777, true, true);
            }
            $width = $faker->numberBetween(100, 400);
            $height = $faker->numberBetween(100, 400);
            $imageUrl = file_get_contents($faker->imageUrl($width, $height));
            $image = $manager->make($imageUrl);
            $image->save(storage_path('app/' . $location . $i . '.original.' . $extension));

            $thumbnail = $manager->make($imageUrl)->resize(null, config('dam.thumbnail.default-width'), function($constraint){
                $constraint->aspectRatio();
            });
            $thumbnail->save(storage_path('app/' . $location . $i . '.thumbnail.' . config('dam.thumbnail.default-extension')));

            $web = $manager->make($imageUrl)->resize(null, config('dam.thumbnail.default-width'), function($constraint){
                $constraint->aspectRatio();
            });
            $web->save(storage_path('app/' . $location . $i . '.web.' . config('dam.web.default-extension')));

            DB::table('assets')->insert(
                [
                    'id' => $i,
                    'user_id' => $faker->numberBetween(1, 2),
                    'product_id' => $faker->randomElement([1, 2]),
                    'creator_id' => $faker->randomElement([1, 2, null]),
                    'assetable_id' => $i,
                    'assetable_type' => \App\Models\Asset\SubAsset\ImageAsset::class,
                    'type' => 'image',
                    'mimetype' => $image->mime(),
                    'extension' => $extension,
                    'size' => $image->filesize(),
                    'original_name' => $faker->words(3, true),
                    'name' => $faker->words(3, true),
                    'description' => $faker->text(500),
                    'available' => $faker->randomElement(['yes', 'no', 'unknown', 'contact']),
                    'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                    'updated_at' => DB::raw('CURRENT_TIMESTAMP')
                ]);
            DB::table('image_assets')->insert(
                [
                    'id' => $i,
                    'height' => $height,
                    'width' => $width,
                ]);

        }

    }
}
