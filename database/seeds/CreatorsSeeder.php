<?php

use Illuminate\Database\Seeder;

class CreatorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('nl_NL');
        for ($i = 0; $i <= 1; $i++) {
            DB::table('creators')->insert(
                [
                    'user_id' => $faker->numberBetween(1, 2),
                    'first_name' => $faker->firstName,
                    'last_name' => $faker->lastName,
                    'function' => '',
                    'email' => $faker->email,
                    'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                    'updated_at' => DB::raw('CURRENT_TIMESTAMP')
                ]);
        }
    }
}
