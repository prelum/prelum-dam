<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->integer('product_id')->index();
            $table->integer('creator_id')->index()->nullable();
            $table->string('type');
            $table->morphs('assetable');
            $table->string('mimetype');
            $table->string('extension');
            $table->integer('size');
            $table->string('original_name');
            $table->string('name');
            $table->text('description');
            $table->string('available');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assets');
    }

}
