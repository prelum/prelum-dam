<?php


return [
    /*
     * Geeft de mogelijkheid om het aantal resultaten per pagina van de index te reguleren
     */
    'pagination' => [
        'options' => [6, 12, 18, 24, 48],
        'default' => 18,
        'assets-per-row' => 6
    ],

    /*
     * Stelt de opties voor het orderen van de resultaten. Opties bestaan uit een array met kolomnaam en order-richting
     */
    'ordering' => [
        'default' => 'last-to-earliest',
        'options' => [
            'small-to-large' => ['size', 'asc'],
            'large-to-small' => ['size', 'desc'],
            'last-to-earliest' => ['created_at', 'asc'],
            'earliest-to-last' => ['created_at', 'desc']
        ]
    ],

    /*
     * Zijn de toegestane types van documenten in de dropzone. Zie http://www.dropzonejs.com/#configuration-options voor alle mogelijkheden
     */
    'allowed_types' => [
        'jpeg' => 'image',
        'jpg' => 'image',
        'psd' => 'image',
        'tiff' => 'image',
        'tif' => 'image',
        'eps' => 'image',
        'png' => 'image',
        'gif' => 'image'
    ],

    /*
     * Geeft de standaard indeling van de asset-thumbnails
     */
    'thumbnail' => [
        'default-width' => 200,
        'default-extension' => 'jpg'
    ],

    /*
     * Geeft de standaard indeling voor de asset webrepresentatie
     */
    'web' => [
        'default-extension' => 'jpg'
    ],

    /*
     * De "enum" velden die via de Enum-trait gecontroleerd kunnen worden
     */
    'enum' => [
        'type' => [
            'image',
            'document',
            'video',
            'audio'
        ],
        'available' => [
            'yes',
            'no',
            'contact',
            'unknown'
        ]
    ]

];