<?php

Route::auth();

// Application routes
Route::group(['middleware' => ['auth']], function(){
    Route::get('/', 'DashboardController@index');

    Route::group(['prefix' => 'assets'], function(){
        Route::get('', 'AssetController@index');
        Route::post('/', 'AssetController@create');
        Route::get('/filter', 'AssetController@filter');
        Route::post('/resultsettings', 'AssetController@changeResultSet');
        Route::get('/{id}', 'AssetController@show');
        Route::post('/{id}/update', 'AssetController@update');
        Route::get('/{id}/edit', 'AssetController@edit');
        Route::put('/{id}', 'AssetController@update');
        Route::get('/{id}/download', 'AssetController@download');
        Route::get('/{id}/delete', 'AssetController@delete');
        Route::delete('/{id}', 'AssetController@destroy');
        Route::group(['prefix' => '/{$asset_id}/comments'], function(){
            Route::get('/create', 'CommentController@create')->name('comment.create');
            Route::post('/', 'CommentController@store');
            Route::get('/{comment_id}/edit', 'CommentController@edit');
            Route::put('/{comment_id}', 'CommentController@update');
            Route::get('/{comment_id}/delete', 'CommentController@delete');
            Route::delete('/{comment_id}', 'CommentController@destroy');
        });
    });

    Route::group(['namespace' => 'Uploads', 'prefix' => 'upload'], function(){
        Route::get('/', 'DropzoneController@index');
        Route::post('/', 'DropzoneController@post');
        Route::get('/batch', 'BatchUploadsController@index');
        Route::post('/batch', 'BatchUploadsController@batchUpload');
        Route::get('/individual/{uuid}', 'IndividualUploadsController@show');
        Route::post('/individual', 'IndividualUploadsController@store');
    });

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){
        Route::get('/users', 'UserAdminController@index');
        Route::get('/products', 'ProductAdminController@index');
        Route::get('/tags', 'TagAdminController@index');
    });

    Route::group(['prefix' => 'users'], function(){
        Route::get('/create', 'UserController@create');
        Route::get('/{id}', 'UserController@show');
        Route::post('/', 'UserController@store');
        Route::get('/{id}/edit', 'UserController@edit');
        Route::put('/{id}', 'UserController@update');
    });

    Route::group(['prefix' => 'creators'], function(){
        Route::get('/create', 'CreatorController@create');
        Route::post('/', 'CreatorController@store');
        Route::get('/{id}/edit', 'CreatorController@edit');
        Route::put('/{id}', 'CreatorController@update');
    });

    Route::group(['prefix' => 'products'], function(){
        Route::get('/create', 'ProductController@create');
        Route::post('/', 'ProductController@store');
        Route::get('/{id}/edit', 'ProductController@edit');
        Route::put('/{id}', 'ProductController@update');
    });
});